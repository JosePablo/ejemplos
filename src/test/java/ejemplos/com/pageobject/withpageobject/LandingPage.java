package test.java.ejemplos.com.pageobject.withpageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandingPage extends AbstractPageObject {
	public LandingPage(WebDriver driver) {
		super(driver);
	}

	@Override
	protected By getElementoPrincipal() {
		return menu;
	}

	By menu = By.id("hmenus");
	By preguntas = By.id("nav-questions");

	/**
	 * Realiza un click en la pregunta
	 * 
	 * @return QuestionsPage la pagina de preguntas
	 * */
	public QuestionsPage clickPregunta() {
		WebElement questionsTab = driver.findElement(preguntas);
		questionsTab.click();
		return new QuestionsPage(driver);
	}

	/**
	 * Verifica si la pregunta es mostrada
	 * 
	 * @return true si la pregunta es mostrada
	 * */
	public Boolean isPreguntaMostrada() {
		List<WebElement> listaPreguntas = driver.findElements(preguntas);
		return listaPreguntas.size() > 0;
	}
}