package test.java.ejemplos.com.pageobject.withpageobject;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ExampleTest extends BaseTest {

	public ExampleTest() {
		super();
	}

	@Test
	public void clickQuestionsTest() {
		LandingPage landingPage = new LandingPage(driver);
		QuestionsPage questionsPage = landingPage.clickPregunta();
		assertTrue("El usuario no se esta mostrando",
				questionsPage.isUserMostrado());
	}

	@Test
	public void isLogoDisplayedTest() {
		LandingPage landingPage = new LandingPage(driver);
		assertTrue("La pregunta no esta sindo mostrada",
				landingPage.isPreguntaMostrada());
	}
}