package test.java.ejemplos.com.pageobject.withpageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class QuestionsPage extends AbstractPageObject {
	public QuestionsPage(WebDriver driver) {
		super(driver);
	}

	@Override
	protected By getElementoPrincipal() {
		return preguntaSeleccionada;
	}

	By preguntaSeleccionada = By.cssSelector(".youarehere #nav-questions");
	By users = By.id("nav-users");

	/**
	 * Verifica que el user este siendo mostrado
	 * 
	 * @return true si el user esta siendo mostrado
	 * */
	public Boolean isUserMostrado() {
		List<WebElement> usersTab = driver.findElements(users);
		return usersTab.size() > 0;
	}
}